import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { MaterialModule } from "../../material.module";
import { CotizacionComponent } from "./cotizacion.component";
import { CreateCotizacionComponent } from "./create/create.component";
@NgModule({
    declarations: [CotizacionComponent, CreateCotizacionComponent],
    imports: [BrowserModule, MaterialModule],
})

export class CotizacionModule { }
