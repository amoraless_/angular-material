import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
@Component({
    selector: 'page-cotizacion-create',
    templateUrl: 'create.template.html'
})
export class CreateCotizacionComponent implements OnInit, AfterViewInit {
    ngOnInit() {

    }
    ngAfterViewInit() {
        $('#elect').select2({
            language: 'es',
    });
    }
}
