import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { MaterialModule } from "../../material.module";
import { DahsboardComponent } from "./dahsboard.component";
@NgModule({
    declarations: [DahsboardComponent],
    imports: [BrowserModule, MaterialModule],
})

export class DahsboardModule { }
