import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { mainViewComponent } from "./main-view.component";
import { MaterialModule } from "../../material.module";
@NgModule({
    declarations: [mainViewComponent],
    imports: [BrowserModule, MaterialModule],
})

export class MainViewModule { }
