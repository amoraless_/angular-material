import { NgModule } from "@angular/core";
import { DahsboardModule } from "./dahsboard/dahsboard.module";
import { CotizacionModule } from "./cotizacion/cotizacion.module";
@NgModule({
    declarations: [],
    imports: [
        DahsboardModule,
        CotizacionModule
    ],
    exports: []
})

export class ViewsModule { }
